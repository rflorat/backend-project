<?php

namespace TypiCMS\Modules\Courses\Models;

use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Core\Models\Base;
use TypiCMS\Modules\History\Traits\Historable;

class Course extends Base
{
    use Historable;
    use PresentableTrait;

    protected $presenter = 'TypiCMS\Modules\Courses\Presenters\ModulePresenter';

    protected $fillable = [
        'name',
        'icon',
        'video',
        'schedule',
        'description',
        'image',
    ];

    protected $appends = [];

    protected $attributes = ['image' => false];
    /**
     * Columns that are file.
     *
     * @var string image
     */
    public $attachments = ['image'];


    /**
     * Get title attribute from translation table
     * and append it to main model attributes.
     *
     * @return string title
     */
    public function getTitleAttribute($value)
    {
        return $value;
    }

}
