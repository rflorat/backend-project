<?php

namespace TypiCMS\Modules\Courses\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Courses\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('courses')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.courses', 'uses' => 'PublicController@form']);
                        $router->get($uri.'/sent', $options + ['as' => $lang.'.courses.sent', 'uses' => 'PublicController@sent']);
                        $router->post($uri, $options + ['as' => $lang.'.courses.store', 'uses' => 'PublicController@store']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/courses', ['as' => 'admin.courses.index', 'uses' => 'AdminController@index']);
            $router->get('admin/courses/create', ['as' => 'admin.courses.create', 'uses' => 'AdminController@create']);
            $router->get('admin/courses/{course}/edit', ['as' => 'admin.courses.edit', 'uses' => 'AdminController@edit']);
            $router->post('admin/courses', ['as' => 'admin.courses.store', 'uses' => 'AdminController@store']);
            $router->put('admin/courses/{course}', ['as' => 'admin.courses.update', 'uses' => 'AdminController@update']);

            /*
             * API routes
             */
            $router->get('api/courses', ['as' => 'api.courses.index', 'middleware' => 'cors', 'uses' => 'ApiController@index']);
            $router->put('api/courses/{course}', ['as' => 'api.courses.update', 'uses' => 'ApiController@update']);
            $router->delete('api/courses/{course}', ['as' => 'api.courses.destroy', 'uses' => 'ApiController@destroy']);
        });
    }
}
