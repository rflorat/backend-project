<?php

namespace TypiCMS\Modules\Courses\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Courses\Events\EventHandler;
use TypiCMS\Modules\Courses\Models\Course;
use TypiCMS\Modules\Courses\Repositories\CacheDecorator;
use TypiCMS\Modules\Courses\Repositories\EloquentCourse;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.courses'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['courses' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'courses');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'courses');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/courses'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        // Honeypot facade
        AliasLoader::getInstance()->alias(
            'Honeypot',
            'Msurguy\Honeypot\HoneypotFacade'
        );

        // Observers
        Course::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Subscribe to events class
         */
        $app->events->subscribe(new EventHandler());

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Courses\Providers\RouteServiceProvider');

        /*
         * Register Honeypot
         */
        $app->register('Msurguy\Honeypot\HoneypotServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Courses\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('courses::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('courses');
        });

        $app->bind('TypiCMS\Modules\Courses\Repositories\CourseInterface', function (Application $app) {
            $repository = new EloquentCourse(new Course());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'courses', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
