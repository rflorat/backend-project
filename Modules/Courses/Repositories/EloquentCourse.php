<?php

namespace TypiCMS\Modules\Courses\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentCourse extends RepositoriesAbstract implements CourseInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
