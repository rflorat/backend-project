<?php

namespace TypiCMS\Modules\Courses\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface CourseInterface extends RepositoryInterface
{
}
