<?php

namespace TypiCMS\Modules\Courses\Composers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;
use TypiCMS\Modules\Core\Composers\BaseSidebarViewComposer;

class SidebarViewComposer extends BaseSidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.contacts'), function (SidebarGroup $group) {
            $group->addItem(trans('courses::global.name'), function (SidebarItem $item) {
                $item->icon = config('typicms.courses.sidebar.icon', 'icon fa fa-street-view');
                $item->weight = config('typicms.courses.sidebar.weight');
                $item->route('admin.courses.index');
                $item->append('admin.courses.create');
                $item->authorize(
                    $this->auth->hasAccess('courses.index')
                );
            });
        });
    }
}
