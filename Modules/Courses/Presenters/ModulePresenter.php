<?php

namespace TypiCMS\Modules\Courses\Presenters;

use TypiCMS\Modules\Core\Presenters\Presenter;

class ModulePresenter extends Presenter
{
    /**
     * Format creation date.
     *
     * @return string
     */
    public function createdAt()
    {
        return $this->entity->created_at->format('d.m.Y');
    }

    /**
     * Get title by concatenate title, first_name and last_name.
     *
     * @return string
     */
    public function title()
    {
        return $this->entity->name;
    }
}
