<?php

namespace TypiCMS\Modules\Courses\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\Dispatcher;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class EventHandler
{
    public function onCreate(Model $model)
    {
        $webmaster = config('typicms.webmaster_email');

        // Send a mail to visitor
        Mail::send('courses::mails.message-to-visitor', ['model' => $model], function (Message $message) use ($model, $webmaster) {
            $subject = '['.TypiCMS::title().'] ';
            $subject .= trans('courses::global.Thank you for your contact request');
            $message->from($webmaster)->to($model->email)->subject($subject);
        });

        // Send a mail to webmaster
        Mail::send('courses::mails.message-to-webmaster', ['model' => $model], function (Message $message) use ($model, $webmaster) {
            $subject = '['.TypiCMS::title().'] ';
            $subject .= trans('courses::global.New courses request');
            $message->from($model->email)->to($webmaster)->subject($subject);
        });
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     *
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('NewCourseRequest', 'TypiCMS\Modules\Courses\Events\EventHandler@onCreate');
    }
}
