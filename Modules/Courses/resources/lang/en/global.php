<?php

return [
    'name'                               => 'Corsi',
    'courses'                           => 'corso|corsi',
    'New'                                => 'Nuovo corso',
    'Edit'                               => 'Modifica il corso',
    'Back'                               => 'Torna ai corsi',
    'Thank you for your course request' => 'Grazie per la richiesta',
    'New course request'                => 'Nuovo corso richiesto',
    'New course request from'           => 'Nuova richiesta di',
    'View online'                        => 'Guarda on-line',
];
