<?php

namespace TypiCMS\Modules\Courses\Http\Controllers;

use TypiCMS\Modules\Courses\Http\Requests\FormRequest;
use TypiCMS\Modules\Courses\Models\Course;
use TypiCMS\Modules\Courses\Repositories\CourseInterface;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function __construct(CourseInterface $course)
    {
        parent::__construct($course);
    }

    /**
     * Create form for a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = $this->repository->getModel();

        return view('core::admin.create')
            ->with(compact('model'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Courses\Models\Course $course
     *
     * @return \Illuminate\View\View
     */
    public function edit(Course $course)
    {
        return view('core::admin.edit')
            ->with(['model' => $course]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Courses\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $course = $this->repository->create($request->all());

        return $this->redirect($request, $course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Courses\Models\Course            $course
     * @param \TypiCMS\Modules\Courses\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Course $course, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $course);
    }
}
