<?php

namespace TypiCMS\Modules\Courses\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'name'     => 'required|max:255',
            'icon'      => 'required|max:255',
        ];
    }
}
