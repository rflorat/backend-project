<?php

namespace TypiCMS\Modules\Contacts\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'title'     => 'required|max:255',
            'date'      => 'required|max:255',
            'schedule'  => 'required|max:255',
            'address'   => 'required|max:255',
        ];
    }
}
