<?php

namespace TypiCMS\Modules\Locations\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'name'     => 'required|max:255',
            'address'      => 'required|max:255',
            'place'  => 'required|max:255',
            'lat'   => 'required|max:255',
            'log'   => 'required|max:255',
        ];
    }
}
