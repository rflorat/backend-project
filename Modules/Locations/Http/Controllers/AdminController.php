<?php

namespace TypiCMS\Modules\Locations\Http\Controllers;

use TypiCMS\Modules\Locations\Http\Requests\FormRequest;
use TypiCMS\Modules\Locations\Models\Location;
use TypiCMS\Modules\Locations\Repositories\LocationInterface;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function __construct(LocationInterface $location)
    {
        parent::__construct($location);
    }

    /**
     * Create form for a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = $this->repository->getModel();

        return view('core::admin.create')
            ->with(compact('model'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Locations\Models\Location $location
     *
     * @return \Illuminate\View\View
     */
    public function edit(Location $location)
    {
        return view('core::admin.edit')
            ->with(['model' => $location]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Locations\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $location = $this->repository->create($request->all());

        return $this->redirect($request, $location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Locations\Models\Location            $location
     * @param \TypiCMS\Modules\Locations\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Location $location, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $location);
    }
}
