<?php

namespace TypiCMS\Modules\Locations\Http\Controllers;

use TypiCMS\Modules\Locations\Http\Requests\FormRequest;
use TypiCMS\Modules\Locations\Repositories\LocationInterface;
use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;

class PublicController extends BasePublicController
{
    protected $form;

    public function __construct(LocationInterface $location)
    {
        parent::__construct($location);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Facades\Response
     */
    public function form()
    {
        return view('locations::public.form');
    }

    /**
     * Display a page when form is sent.
     *
     * @return \Illuminate\Support\Facades\Response
     */
    public function sent()
    {
        if (session('success')) {
            return view('locations::public.sent');
        }

        return redirect(url('/'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Support\Facades\Response
     */
    public function store(FormRequest $request)
    {
        $data = [];
        foreach ($request->all() as $key => $value) {
            $data[$key] = strip_tags($value);
        }
        $location = $this->repository->create($data);

        event('NewLocationRequest', [$location]);

        return redirect()->route(config('app.locale').'.locations.sent')
            ->with('success', true);
    }
}
