<?php

return [
    'name'                               => 'Sedi',
    'locations'                           => 'sede|sedi',
    'New'                                => 'Nuova sede',
    'Edit'                               => 'Modifica sede',
    'Back'                               => 'Torna alle sedi',
    'Thank you for your location request' => 'Grazie per la richiesta',
    'New location request'                => 'Nuova sede richiesta',
    'New location request from'           => 'Nuova richiesta di',
    'View online'                        => 'Guarda on-line',
];
