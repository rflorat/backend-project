<?php

namespace TypiCMS\Modules\Locations\Models;

use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Core\Models\Base;
use TypiCMS\Modules\History\Traits\Historable;

class Location extends Base
{
    use Historable;
    use PresentableTrait;

    protected $presenter = 'TypiCMS\Modules\Locations\Presenters\ModulePresenter';

    protected $fillable = [
        'name',
        'address',
        'city',
        'place',
        'description',
        'lat',
        'log',
        'phone',
        'image'
    ];

    protected $appends = [];

    /**
     * Columns that are file.
     *
     * @var array
     */
    public $attachments = [
        'image',
    ];

    /**
     * Get title attribute from translation table
     * and append it to main model attributes.
     *
     * @return string title
     */
    public function getTitleAttribute($value)
    {
        return $value;
    }

}
