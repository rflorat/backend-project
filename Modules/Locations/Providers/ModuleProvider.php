<?php

namespace TypiCMS\Modules\Locations\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Locations\Events\EventHandler;
use TypiCMS\Modules\Locations\Models\Location;
use TypiCMS\Modules\Locations\Repositories\CacheDecorator;
use TypiCMS\Modules\Locations\Repositories\EloquentLocation;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.locations'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['locations' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'locations');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'locations');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/locations'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        // Honeypot facade
        AliasLoader::getInstance()->alias(
            'Honeypot',
            'Msurguy\Honeypot\HoneypotFacade'
        );

        // Observers
        Location::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Subscribe to events class
         */
        $app->events->subscribe(new EventHandler());

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Locations\Providers\RouteServiceProvider');

        /*
         * Register Honeypot
         */
        $app->register('Msurguy\Honeypot\HoneypotServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Locations\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('locations::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('locations');
        });

        $app->bind('TypiCMS\Modules\Locations\Repositories\LocationInterface', function (Application $app) {
            $repository = new EloquentLocation(new Location());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'locations', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
