<?php

namespace TypiCMS\Modules\Locations\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Locations\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('locations')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.locations', 'uses' => 'PublicController@form']);
                        $router->get($uri.'/sent', $options + ['as' => $lang.'.locations.sent', 'uses' => 'PublicController@sent']);
                        $router->post($uri, $options + ['as' => $lang.'.locations.store', 'uses' => 'PublicController@store']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/locations', ['as' => 'admin.locations.index', 'uses' => 'AdminController@index']);
            $router->get('admin/locations/create', ['as' => 'admin.locations.create', 'uses' => 'AdminController@create']);
            $router->get('admin/locations/{location}/edit', ['as' => 'admin.locations.edit', 'uses' => 'AdminController@edit']);
            $router->post('admin/locations', ['as' => 'admin.locations.store', 'uses' => 'AdminController@store']);
            $router->put('admin/locations/{location}', ['as' => 'admin.locations.update', 'uses' => 'AdminController@update']);

            /*
             * API routes
             */
            $router->get('api/locations', ['as' => 'api.locations.index', 'middleware' => 'cors', 'uses' => 'ApiController@index']);
            $router->put('api/locations/{location}', ['as' => 'api.locations.update', 'uses' => 'ApiController@update']);
            $router->delete('api/locations/{location}', ['as' => 'api.locations.destroy', 'uses' => 'ApiController@destroy']);
        });
    }
}
