<?php

namespace TypiCMS\Modules\Locations\Composers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;
use TypiCMS\Modules\Core\Composers\BaseSidebarViewComposer;

class SidebarViewComposer extends BaseSidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.contacts'), function (SidebarGroup $group) {
            $group->addItem(trans('locations::global.name'), function (SidebarItem $item) {
                $item->icon = config('typicms.locations.sidebar.icon', 'icon fa fa-map-marker');
                $item->weight = config('typicms.locations.sidebar.weight');
                $item->route('admin.locations.index');
                $item->append('admin.locations.create');
                $item->authorize(
                    $this->auth->hasAccess('locations.index')
                );
            });
        });
    }
}
