<?php

namespace TypiCMS\Modules\Locations\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentLocation extends RepositoriesAbstract implements LocationInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
