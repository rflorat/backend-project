<?php

namespace TypiCMS\Modules\Locations\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface LocationInterface extends RepositoryInterface
{
}
