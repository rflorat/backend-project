@section('js')
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/admin/form.js') }}"></script>
@endsection

@include('core::admin._buttons-form')

{!! Honeypot::generate('my_name', 'my_time') !!}
{!! BootForm::hidden('language')->value(isset($model->language) ? $model->language : config('app.locale')) !!}
{!! BootForm::hidden('id') !!}


<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">@lang('global.Content')</a>
    </li>
    <li>
        <a href="#tab-images" data-target="#tab-images" data-toggle="tab">@lang('global.Images')</a>
    </li>
</ul>

<div class="tab-content">

    <div class="tab-pane fade in active" id="tab-content">
        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.name'), 'name') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.address'), 'address') !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.city'), 'city') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.place'), 'place') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.description'), 'description') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.latitude'), 'lat') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.longitude'), 'log') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! BootForm::text(trans('validation.attributes.phone'), 'phone') !!}
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="tab-images">
         @include('core::admin._image-fieldset', ['field' => 'image'])
    </div>

</div>

{!! BootForm::hidden('my_time')->value(Crypt::encrypt(time()-60)) !!}
