<?php

return [

    /*
    |--------------------------------------------------------------------------
    | EN Global TypiCMS Language Lines
    |--------------------------------------------------------------------------
    */
    'languages' => [
        'es'  => 'Spanish',
        'en'  => 'English',
        'all' => 'All languages',
    ],
    'form' => [
        'page content' => 'page content',
        'meta data'    => 'meta data',
        'options'      => 'options',
    ],
    'menus' => [
        'content'  => 'Content',
        'users'    => 'Users and groups',
        'media'    => 'Media',
        'contacts' => 'Contacts',
    ],
    'Index'        => 'List',
    'Create'       => 'Create',
    'Store'        => 'Store',
    'Update'       => 'Update',
    'Delete'       => 'Delete',
    'Select all'   => 'Select all',
    'Deselect all' => 'Deselect all',
    'Online'       => 'Online',
    'Offline'      => 'Offline',
    'View'         => 'View',
    'Sort'         => 'Sort',
    'Edit'         => 'Edit',
    'Search'       => 'Ricerca',
    'Not found'    => 'Not found',

    'Yes' => 'Yes',
    'No'  => 'No',

    'Home'                  => 'Home',
    'En ligne/Hors ligne'   => 'Online/Offline',
    'No default page found' => 'No default page found',
    'No file'               => 'No file',
    'Settings'              => 'Settings',
    'Admin side'            => 'Admin side',
    'View website'          => 'View website',

    'Mandatory fields' => 'Mandatory fields',

    // Tabs
    'Content'   => 'Contenuto',
    'Meta'      => 'Meta',
    'Options'   => 'Opzioni',
    'Galleries' => 'Gallerie',
    'Files'     => 'File',
    'Images'    => 'Immage',
    'Info'      => 'Informazioni',

    'Toggle navigation' => 'Toggle navigation',

    'Items sorted' => 'Items sorted.',

    'Security token expired. Please, repeat your request.' => 'Security token expired. Please, repeat your request.',
];
